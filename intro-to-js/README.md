# Introduction to the JavaScript

In this video you will be learning about what JavaScript is and how it fits into web development. You will also be exposed to the basics of JavaScript and how to write and evaluate your own code.

## Video

[![intro-to-js](https://img.youtube.com/vi/nskf95tCpQg/0.jpg)](https://youtu.be/nskf95tCpQg "Introduction to JavaScript")

## Resources

* [Eloquent Javascript](http://eloquentjavascript.net/) - I highly suggest completing Chapters 1 through 6!
* [Codewars](https://www.codewars.com)

## Questions/Comments?

Make an issue in the repository's [issue tracker](https://gitlab.technicity.io/fullstack-foundations/intro-to-js/issues).

