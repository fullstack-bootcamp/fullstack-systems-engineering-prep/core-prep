// Functions

function helloWorld() {
  console.log("Hello world!");
}

helloWorld();

// Arrays

var animals = ["dog", "cat", "horse"];

console.log("These are the animals: ", animals);

// For loops

function printTheAnimals(animals) {
  for (var i = 0; i < animals.length; i++) {
    console.log(i, animals[i]);
  }
}

printTheAnimals(animals);

// While loops

var counter = 5;

while (counter >= 0) {
  console.log(counter);
  counter--;
}

// If statements

var counter = 5;

while (counter >= 0) {
  if (counter === 0) {
    console.log("Lift off!!!");
  } else {
    console.log(counter);
  }
  counter--;
}

// Math

function doubler(numToDouble) {
  return numToDouble * 2;
}

console.log("15 doubled is", doubler(15));

// PI * r^2
function findCircleArea(radius) {
  return Math.PI * Math.pow(radius, 2);
}

console.log("Area of radius 5: ", findCircleArea(5));
