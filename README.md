# Fullstack Systems Engineering Prep Content

The preparation content is designed to give you some background and exposure to the concepts covered in the bootcamp.

While order does not particularly matter, this order may provide the best basis for learning:

0. intro-to-cli-git
1. intro-to-python
2. intro-to-js
3. intro-to-html
4. intro-to-css
5. datastructures-algorithms-oo
6. networking-basics
7. docker basics
8. advanced-js
