import socket

# create socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
host = '127.0.0.1'  # localhost
port = 8000

s.bind((host, port))

s.listen(1)

while True:
    print('Waiting for Connection...')
    c, addr = s.accept()  # establish connection with client
    print('Got connection!')

    c.send(b'Thanks for connecting!')

    c.close()
