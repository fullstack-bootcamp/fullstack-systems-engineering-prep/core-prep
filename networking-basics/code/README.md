# Running `server.py` and `client.py`

First, clone the repository:
```bash
git clone https://gitlab.technicity.io/fullstack-foundations/networking-basics
```

Running `server.py` and `client.py` is easy! Simply `cd` into your cloned git `code` folder, open up a terminal, and start `server.py`:

```bash
python server.py
```

Next, open up a second terminal, and run `client.py`:

```bash
python client.py
```

You should be able to run `client.py` as many times as you want.

