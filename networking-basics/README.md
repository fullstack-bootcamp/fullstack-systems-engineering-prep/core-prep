# Networking Basics
This module is intended to give you very high level exposure to networking basics that drive everythin we do as Fullstack Devs.

You should feel very comfortable with the concepts covered in this preparation material (it is very basic).  This material will come in handy when we start talking about DevOps, deployment, and microservice app development (all things that encompass what we do).

## Video
Here are the [video](https://youtu.be/8i5RB9vXIE4) and the [presentation](https://gitlab.technicity.io/fullstack-foundations/networking-basics/blob/master/Networking.Internet.Basics.pdf):

[![Networking Basics](http://img.youtube.com/vi/8i5RB9vXIE4/0.jpg)](https://youtu.be/8i5RB9vXIE4 "Networking Basics")

## Resources

 - [DNS](https://scotch.io/tutorials/dns-explained-how-your-browser-finds-websites)
 - [TCP/IP](https://en.wikipedia.org/wiki/Internet_protocol_suite)
 - [HTTPS](https://en.wikipedia.org/wiki/HTTPS)
 - [Root Servers](https://www.iana.org/domains/root/servers)

## Questions/Comments?
Make an issue in this repo's [issue tracker](https://gitlab.technicity.io/fullstack-foundations/networking-basics/issues) and assign the label `question` to it.