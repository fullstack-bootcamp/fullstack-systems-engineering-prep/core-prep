# The callstack

As you have been learning, there can be multiple execution contexts that may be tracked during a JS program. Everytime a function is called, a new context is created. To keep track of these, your computer stores these contexts in the *call stack*. 

## Resources

Approach these resources in order:

* [Eloquent Javascript: Ch 3 Functions](http://eloquentjavascript.net/03_functions.html) - Just read the section called "The Call Stack"
* [Codecademy Video 1](https://youtu.be/jT0USJeNFEA)
* [Codecademy Video 2](https://youtu.be/F9o4CPcDq18)
* [Codecademy Video 3](https://youtu.be/FCAOcYazy9c)
* [Eloquent Javascript: Ch 3 Functions](http://eloquentjavascript.net/03_functions.html) - Just read the section called "Recursion" (you don't need to master this section, just understand what recursion is in general)

## Main goals

* Have a deeper understanding of the execution context
* What is the callstack? How does that relate to the execution context?

Bonus:

* What is recursion? What are some benefits of using it over iteration?