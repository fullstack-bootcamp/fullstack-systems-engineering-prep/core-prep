# Intermediate and Advanced JavaScript Concepts

Here you will find material on concepts that will build on the basics of JavaScript that you have previously learned. The content is ordered in a progressive fashion, so start from the top and work down. We are creating this content on a rolling basis, so we'll let you know when new content is available!

## Content

This content is arrange to build on top of each other, so please complete these in succession. You don't need to master these immediately, just being knowledgeable and conversational of the content is good enough. We'll go through these in more depth as we progress in the curriculum.

* [Scope and Closure](./scope-and-closure.md)
* [Objects in JavaScript](./objects-in-js.md)
* [Execution context](./execution-context.md)
* [The callstack](./the-callstack.md)
* [Inheritance](./inheritance.md)
* [Asynchronous programming](./async-programming.md)

## Questions and Comments

If you see a typo or an error, or have any questions, please make an issue in the [issue tracker](https://gitlab.technicity.io/fullstack-foundations/advanced-js/issues)! 