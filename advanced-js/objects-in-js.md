# Objects in JavaScript

The objective here is to learn everything about JS objects. This is an extremely important data type in JavaScript due to its flexibility and utility. Use the following resources to learn more!

## Resources

* [MDN: Working with objects](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Working_with_Objects) - Stop after you read the section called "Enumerate the properties of an object"
* [Eloquent JavaScript: Ch 4 Data Structures: Objects and Arrays](http://eloquentjavascript.net/04_data.html) - Stop after you read the section called "Mutability"

## Main goals

Here are some concepts that you should know:

* How to create objects
* How to set properties on objects (dot versus bracket notation)
* Listing properties on objects, and looping through properties
* Creating object methods

Bonus:

* Mutability (what is it? how are object data structures different than, say, numbers or strings)