# The execution context

The execution context is a very important fundamental part of JS. It describes the environment in which your code in run. Knowing which environment gets more complicated and messy as your code becomes more complex. But, it is very important to know this context to be able to confidently know how your code will act and perform.

## Resources

* [Codecademy - Video 1](https://youtu.be/KYFTXszMQdk?t=6m15s)
* [Codecademy - Video 2](https://youtu.be/byiUGKVeJuY)
* [Codecademy - Video 3](https://youtu.be/FJNRmUdLlfw)
* [Codecademy - Video 4](https://youtu.be/FCAOcYazy9c)

### Good supplementary text sources

* [David Shariff's blog Pt 1](http://davidshariff.com/blog/what-is-the-execution-context-in-javascript/)
* [David Shariff's blog Pt 2](http://davidshariff.com/blog/javascript-scope-chain-and-closures/)

## Main goals

Here are some concepts you should know:

* What is an execution context (global context, local context)
* How is a context created?
* What variables can each context access?

Bonus:

* What is variable hoisting?