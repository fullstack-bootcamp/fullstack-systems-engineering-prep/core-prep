# Asynchronous programming

Asynchronous programming is a powerful part of JavaScript, but it can be very confusing for beginners in the language. Asynchronous code allows your programs to efficiently execute code without waiting for certain operations to complete before starting others.

## Resources

* [Pluralsight - Introduction to Asynchronous JavaScript](https://www.pluralsight.com/guides/front-end-javascript/introduction-to-asynchronous-javascript) - Stop after reading the section called "Callback functions"
* [The Net Ninja - Asynchronous JavaScript Playlist](https://www.youtube.com/playlist?list=PL4cUxeGkcC9jAhrjtZ9U93UMIhnCc44MH) - Only watch up until Promises (Video 4)
* [Funfunfunction - Promises](https://youtu.be/2d7s3spWAzo) - Warning: uses ES5 syntax, so his functions might look different from what you've seen or are used to, but the video will still make sense
* [Philip Robers: What the heck is the event loop anyways?](https://youtu.be/8aGhZQkoFbQ) - Bonus content

## Main goals

* What does it mean for code to run asynchronously?
* What's the difference between synchronous and asynchronous code?
* What are callbacks? What are promises? How are they different?

Bonus:

* Can you explain how the event loop works?
