# Scope and Closure

## Local and Global Scope

There is a concept of a local and global scope for variables in JavaScript.

* *global*: Variables declared outside of any function in your document code. Global variables are visible/accessible throughout your program, by any function.
* *local*: Variables are considered local if there are declared inside a function body.

Consider the following code:

```js
var myVar = 'outside world';

function myFunction () {
  var myVar = 'inside world';
}

console.log(myVar); 
// outside world
```

The above code declares the variable `myVar` twice, once in the global scope and another time in the local scope of `myFunction`.

You may think this code should log out `inside world`, but it doesn't because that string was only declared for the local instance variable of `myFunction`. When you log out `myVar` outside the function (the global scope), it will refer to the string `outside world`.

Why is there such thing as scoping? Imagine if every variable in your program could be seen anywhere. It would be very easy to accidentally have variables with the same names, which would cause difficult and hard to trace bugs in your program.

## Lexical Scope

In JavaScript, you can create functions within other functions. And with each new function, a new scope for variables in created. So how to you know which variables are accessible to which scope? It depends on the location of the function itself (hence the name *lexical* scope). 

A given function's local scope can see all the scopes that contain it. That is, if `functionB` is instantiated inside `functionA`, then `functionA` has access to the variables of `functionB`.

Let's look at a contrived example:

```js
var animals = 'all animals';

function runnerA () {
  var mammals = 'all mammals';
  console.log(animals, mammals); 
  
  function runnerB () {
    var monkeys = 'all monkeys';
    console.log(animals, mammals, monkeys); 
  }
  
  function runnerC () {
    var dogs = 'all dogs';
    console.log(animals, mammals, dogs) 
  }
  
  runnerB()
  runnerC()
}

runnerA()
// all animals all mammals
// all animals all mammals all monkeys
// all animals all mammals all dogs
```

As you may expect, for every log within a function, we are able to access variables from the funciton's local scope as well as its surrounding scopes, all the way up to the global scope.

What if we tried to log `dogs` within `runnerB`? We would get the following:

```js
ReferenceError: dogs is not defined
```

This is because the scoped variables that `runnerB` has access to does not include `dogs`. `dogs` is only declared in the scope of `runnerC`, which does not surround `runnerB`.

## Function Declarations

There are two ways of creating functions on JavaScript. The first is creating a variable and setting a function to it like this:

```js
var addTwo = function (num) {
  return num + 2;
}
```

And then there is a *function declaration*. Which is shorter in syntax:

```js
function addTwo (num) {
  return num + 2;
}
```

These may seem like insignificant differences between the two, and that is mostly correct. They do very much act the same, except for one key detail. Consider the following code:

```js
willILog()
// Yes!

function willILog() {
  console.log('Yes!')
}
```

Now you may be thinking, if JavaScript is interpreted (read line-by-line), how does it call a function that doesn't "exist" yet? Well, any *function declarations* are *hoisted* to the top of their scope and therefore can be used by all the code in that scope. This is helpful because it gives you the freedom to place your function declarations anywhere in the scope.

## Passing Functions As Values

Just like variables can be passed around in JavaScript, functions can as well. When you declare a function it creates the function *and* a named variable for that function. So in the following example, we declare a function `yell` and then pass it into another function `iCallTheFunction` as a parameter.

```js
function yell() {
  console.log('AHHHHH!');
}

function iCallTheFunction(functionToCall) {
  console.log('Calling the function!')
  functionToCall()
}

iCallTheFunction(yell);
// Calling the function!
// AHHHHH!
```

## Closure

So now we've learned that nested functions have access to their surrounding scopes. But, what happens if the variables in their outer scopes no longer exists? Can you still use those variables? An example of that is below.

```js
function secretCreator() {
  var mySecret = 'I like T Swift';
  
  return function tellSecret () {
    console.log(mySecret);
  }
}

var aSecret = secretCreator();
aSecret()
```

When `secretCreator` is called, is creates a new function called `tellSecret`. Inside `tellSecret`, it references a variable called `mySecret` in its parent scope. When we instantiate (execute) `secretCreator` and set it to the variable `aSecret`, the function creates a new scope, returns a new function, and then the scope is gone. But the big question is can we still access `mySecret`? Thankfully, yes we can. This is called *closure*.

When we execute `aSecret` we get back `I like T Swift`. The function `tellSecret` "closes over" the variable `mysecret`.

Here's a slightly more complicated example:

```js
function runner () {
  var counter = 0;
  
  var testFn1 = function () {
    for (let i = 0; i < 3; i++) {
      counter++;
      console.log(counter)   
    }
  }
  
  var testFn2 = function () {
    for (let i = 0; i < 3; i++) {
      counter++;
      console.log(counter)   
    }
  }
  
  return {
    testFn1,
    testFn2
  }
}

var objOfFunctions = runner()

objOfFunctions.testFn1()
// 1
// 2 
// 3 
objOfFunctions.testFn2()
// 4 
// 5 
// 6 
```

In here, we create a `counter` variable that is in the `runner` scope. Then, we create two functions `testFn1` and `testFn2` that use closure on the `counter` variable and return those functions. Outside in the global scope, we call both those functions. We might think that each one should log out `1, 2, 3`, but actually the second function logs `4, 5, 6`. This is because they are referencing ("closing over") the *same* variable.

Closure can be difficult to learn at first, but it is an extremely powerful and useful feature in JavaScript. The more you write JavaScript, the easier it is to know when to use closure or not, so no worries if it's not totally clear right now!