# Inheritance in JS

JavaScript differs from many programming languages in that it not necessarilyprovide a `class` implementation. But, thanks to the power of JavaScript objects, we can achieve very similar behavior. Checkout the resources to learn more about JS objects and the prototype chain.

## Resources

* [Funfunfunction - Composition vs Inheritance](https://youtu.be/wfMtDGfHWpA)
* [MDN - Inheritance and the prototype chain](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Inheritance_and_the_prototype_chain)
* [MDN - Inheritance](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/Inheritance)

## Main goals

* What is function composition? What is function inheritance?
* How does prototypal inheritance work in JS?
* How do I create an object that inherits from another object?

