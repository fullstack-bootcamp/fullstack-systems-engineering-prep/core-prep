# Introduction to HTML

In this video, you'll gain an understanding of what HTML is and how it fits into web development. You'll also learning the basic elements of HTML and how to structure your own web page.

## Video

[![intro-to-html](https://img.youtube.com/vi/-8nVfKlZOQY/0.jpg)](https://youtu.be/-8nVfKlZOQY "Introduction to HTML")

## Resources

* [Codecademy](https://www.codecademy.com/learn/learn-html)
* [Tutorials Point](https://www.tutorialspoint.com/html/)

## Questions/Comments?

Make an issue in the repository's [issue tracker](https://gitlab.technicity.io/fullstack-foundations/intro-to-html/issues).

