# Introduction to the Command Line Interface and Git

![xkcd comic](https://imgs.xkcd.com/comics/tar.png)

In this video, you'll learn about the Command Line Interface (CLI) and basic
commands to get you started with using it. You will also learn about Git, one of
the most commonly used version control systems in the industry.

## Video

### Introduction to the Command Line Interface

[![intro-to-cli](https://img.youtube.com/vi/nAkF9Ss7vRc/0.jpg)](https://youtu.be/nAkF9Ss7vRc "Introduction to the Command Line Interface")

### Introduction to Git

[![intro-to-git](https://img.youtube.com/vi/cjQDk86ndow/0.jpg)](https://youtu.be/cjQDk86ndow "Introduction to Git")

## Resources

### Bash

* [Basic bash commands](http://www.linfo.org/command_index.html)
* [Advanced bash commands](http://azer.bike/journal/10-linux-commands-every-developer-should-know/)

### Git

* [GitHub interactive tutorial](https://try.github.io/levels/1/challenges/1)
* [Git Tower guides](https://www.git-tower.com/learn/)

## Questions/Comments?

Make an issue in the repository's
[issue tracker](https://gitlab.technicity.io/fullstack-foundations/intro-to-cli-git/issues).
