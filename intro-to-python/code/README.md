# Running `first_program.py`

First, clone the repository and go to the `code` folder:

```bash
git clone https://gitlab.technicity.io/fullstack-foundations/intro-to-python
cd code
```

To run the program, you must have `virtualenv` installed.

```bash
sudo pip install virtualenv
```

Once `virtualenv` is installed, you must activate the environment:

```bash
virtualenv venv
source venv/bin/activate
```

Then install the dependencies:
```bash
pip install -r requirements.txt
```

and finally, run the file:
```bash
python first_program.py
```


