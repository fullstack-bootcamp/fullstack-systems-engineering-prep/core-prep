# Introduction to Python
This module is intended to give you exposure to python and some of the python based tools we will be using in the Fullstack Dev curriculum.

There is no possible way to cover everything python in a short 30 minute video, so if you feel uncomfortable with python, we highly suggest doing your own research, running some examples, and writing code.

## Video
Here are the [video](http://www.youtube.com/watch?v=HWDE5m9aT-8) and the [presentation](https://gitlab.technicity.io/fullstack-foundations/intro-to-python/blob/master/Introduction.to.Python.pdf):

[![intro-to-python](http://img.youtube.com/vi/HWDE5m9aT-8/0.jpg)](http://www.youtube.com/watch?v=HWDE5m9aT-8 "Introduction to Python")

## Resources

 - [Official PyPi Package Manager](https://pypi.python.org/pypi)
 - [Install Python 3 on Ubuntu](https://www.digitalocean.com/community/tutorials/how-to-install-python-3-and-set-up-a-local-programming-environment-on-ubuntu-16-04)
 - [Virtual Environments](http://docs.python-guide.org/en/latest/dev/virtualenvs/)
 - [General Exercises and Practice](https://learnpythonthehardway.org/)

## Questions/Comments?
Make an issue in this repo's [issue tracker](https://gitlab.technicity.io/fullstack-foundations/intro-to-python/issues) and assign the label `question` to it.