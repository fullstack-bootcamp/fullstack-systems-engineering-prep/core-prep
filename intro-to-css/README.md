# Introduction to the CSS 

In this video, you will be learning the basics of how to style your web pages with CSS. 

## Video

[![intro-to-css](https://img.youtube.com/vi/bc58be_KHH8/0.jpg)](https://youtu.be/bc58be_KHH8 "Introduction to CSS")

## Resources

* [Codecademy Tutorial](https://www.codecademy.com/learn/learn-css)
* [MDN References and Tutorials](https://developer.mozilla.org/en-US/docs/Web/CSS)

## Questions/Comments?

Make an issue in the repository's [issue tracker](https://gitlab.technicity.io/fullstack-foundations/intro-to-css/issues).

