# Docker Basics

This module is intended to give you very high level exposure to container and Docker basics that drive everythin we do as Fullstack Devs.

## Video

Here is the [video](https://vimeo.com/430148191) and the [presentation](https://gitlab.com/fullstack-bootcamp/fullstack-systems-engineering-prep/core-prep/-/blob/master/docker/Containerization.FSSE.pdf).

Video password: `FSSE-2020`

## Resources

- [Docker](https://docker.com)
- [Docker Hub](https://hub.docker.com)

## Questions/Comments?

Make an issue in this repo's [issue tracker](https://gitlab.technicity.io/fullstack-foundations/docker/issues) and assign the label `question` to it.
