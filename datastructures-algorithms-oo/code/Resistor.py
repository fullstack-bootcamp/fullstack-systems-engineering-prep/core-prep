""" Resistor Class modeling an electrical resistor component"""


class Resistor(object):
    """ Models a standard electrical circuit resistor """

    def __init__(self, R=0.001):
        """ Constructor for a resistor object """
        self.resistance = R

    def compute_voltage(self, i):
        """ computes the voltage over a resistor"""
        return self.resistance * i

    def compute_current(self, v):
        """ computes the current over a resistor """
        return v / self.resistance

    def get_resistance(self):
        """ Retrieves the given resistance on the resistor """
        return self.resistance
