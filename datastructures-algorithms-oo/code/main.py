""" Main file to run Resistor """

from Resistor import Resistor

r = Resistor(0.1)

u = r.compute_current(12.2)

print('Current through the resistor is {0} amps.'.format(u))
