# Instantiating a Resistor Object

Clone the project:

```bash
git clone https://gitlab.technicity.io/fullstack-foundations/datastructures-algorithms-oo.git
```
and `cd code` into the `code` folder.

Then, simply run the python `main.py` file:

```bash
python main.py
```

The `main.py` file imports `Resistory.py` that holds the Resistor class.