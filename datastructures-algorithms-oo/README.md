# Datastructures, Algorithms, and Object Oriented Programming
This module is intended to give you exposure to some of the datastructures, algorithms, and Object Oriented concepts that we will be dealing with in Fullstack Development.

The concepts in this preparatory overview are designed to help you orient yourself to some of the things we will use in full stack development.  As with all these prep concepts, we encourage you to research these things yourself and get comfortable with the concepts.

## Video
Here are the [video](https://youtu.be/YSRJUk9WaVo) and the [presentation](https://gitlab.technicity.io/fullstack-foundations/datastructures-algorithms-oo/blob/master/Data.Structures.Algorithms.Object.Oriented.Programming.pdf):

[![Datastructures, Algorithms, and Object Oriented Programming](http://img.youtube.com/vi/YSRJUk9WaVo/0.jpg)](https://youtu.be/YSRJUk9WaVo "Datastructures, Algorithms, and Object Oriented Programming")

## Resources

 - [Object Oriented Programming](https://en.wikipedia.org/wiki/Object-oriented_programming)
 - [OO in Python](https://python.swaroopch.com/oop.html)
 - [OO in Javascript](https://medium.com/@justtoconfirm/object-oriented-javascript-d61fe073ca86)
 - [Datastructures in Python](http://interactivepython.org/runestone/static/pythonds/index.html)

## Questions/Comments?
Make an issue in this repo's [issue tracker](https://gitlab.technicity.io/fullstack-foundations/datastructures-algorithms-oo/issues) and assign the label `question` to it.